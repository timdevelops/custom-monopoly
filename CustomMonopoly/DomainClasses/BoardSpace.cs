﻿using CustomMonopoly.DomainClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomMonopoly
{
    public class BoardSpace
    {
        public string Title { get; set; }
        public int SpaceId { get; set; }
        public PropertyType Type { get; set; }

        public BoardSpace (String title, PropertyType type = PropertyType.Residential)
        {
            Title = title;
            MonopolyGame.SpaceCount++;
            SpaceId = MonopolyGame.SpaceCount;
            Type = type;
        }

        public void ToString()
        {
            Console.WriteLine($"BoardSpace (Title = {Title}, SpaceId = {SpaceId}, Type = {Type})");
        }
    }
}
