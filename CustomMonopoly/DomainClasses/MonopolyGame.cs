﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomMonopoly.DomainClasses
{
    class MonopolyGame
    {
        public static int SpaceCount = 0;
        public static List<BoardSpace> Spaces = new List<BoardSpace>();
        public static List<Player> Players = new List<Player>();
        public static Player CurrentTurn { get; set; }

        public static DiceRoll CurrentDiceRoll {get;set;}

        public MonopolyGame()
        {
            SetupBoard();
            PrintBoard();
            CreatePlayers();

        }

        public void FindProperty(int SpaceNumber)
        {
            CurrentTurn.CurrentSpaceProperty = Spaces[SpaceNumber];
            Spaces[SpaceNumber].ToString();
        }

        public DiceRoll RollDice()
        {
            Random r = new Random();
            CurrentDiceRoll = new DiceRoll();
            
            if (RollLoopsBoard(CurrentDiceRoll.Total))
            {
                int totalRoll = CurrentDiceRoll.Total;
                int distanceFromGo = Spaces.Count - CurrentTurn.CurrentSpace;

                int diceDifference = (totalRoll-distanceFromGo);
                CurrentTurn.CurrentSpace = diceDifference;
                FindProperty(CurrentTurn.CurrentSpace);
            }
            else
            {
                CurrentTurn.CurrentSpace += CurrentDiceRoll.Dice1 + CurrentDiceRoll.Dice2;
                FindProperty(CurrentTurn.CurrentSpace);
            }

            return CurrentDiceRoll;
        }

        private bool RollLoopsBoard(int roll)
        {
            int distanceFromGo = Spaces.Count - CurrentTurn.CurrentSpace;
            if (roll >= distanceFromGo)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public Player GetCurrentTurn()
        {
            return CurrentTurn;
        }

        public void CreatePlayers()
        {
            Players.Add(new Player("Tim", 260));
            if (Players.Count > 0)
            {
                CurrentTurn = Players[0];
            }
        }

        public void PrintBoard()
        {
            foreach(var Space in Spaces)
            {
                if (Space.GetType() == typeof(Property))
                {
                    Property ps = (Property)Space;
                    ps.ToString();
                }
                else
                {
                    Space.ToString();
                }
            }
        }

        public void SetupBoard()
        {
            AddBoardSpace(new BoardSpace("GO", PropertyType.Go));
            AddBoardSpace(new Property("Old Kent Road", Colour.Brown, 60, new int[6] { 10, 20, 30, 40, 50, 60 }));
            AddBoardSpace(new BoardSpace("Community Chest", PropertyType.CommunityChest));
            AddBoardSpace(new Property("Whitechapel Road", Colour.Brown, 60, new int[6] { 20, 30, 40, 50, 60, 70 }));
            AddBoardSpace(new TaxSpace("Income Tax", 100));
            AddBoardSpace(new Property("Kings Cross Station", Colour.Black, 200, new int[6] { 30, 40, 50, 60, 70, 80 }));

            AddBoardSpace(new Property("The Angel Islington", Colour.LightBlue, 100, new int[6] { 40, 50, 60, 70, 80, 90 }));
            AddBoardSpace(new BoardSpace("Chance", PropertyType.Chance));
            AddBoardSpace(new Property("Euston Road", Colour.LightBlue, 100, new int[6] { 50, 60, 70, 80, 90, 100 }));
            AddBoardSpace(new Property("Pentonville Road", Colour.LightBlue, 120, new int[6] { 100, 200, 300, 400, 500, 600 }));
            AddBoardSpace(new BoardSpace("In Jail", PropertyType.Jail));
            AddBoardSpace(new Property("Pall Mall", Colour.Pink, 140, new int[6] { 10, 20, 30, 40, 50, 60 }));
        }

        public void AddBoardSpace(BoardSpace bs)
        {
            Spaces.Add(bs);
            SpaceCount++;
        }
    }
}
