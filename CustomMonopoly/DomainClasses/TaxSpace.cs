﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomMonopoly.DomainClasses
{
    public class TaxSpace : BoardSpace
    {
        public int Cost { get; set; }
        public TaxSpace(string title, int Cost) : base(title, PropertyType.Tax)
        {
            this.Cost = Cost;
        }
    }
}
