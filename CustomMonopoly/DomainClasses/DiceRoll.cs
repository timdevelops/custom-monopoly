﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomMonopoly.DomainClasses
{
    class DiceRoll
    {
        public int Dice1 { get; set; }
        public int Dice2 { get; set; }
        public int Total { get; set; }

        public DiceRoll()
        {
            Random r = new Random();
            Dice1 = r.Next(1, 6);
            Dice2 = r.Next(1, 6);
            Total = Dice1 + Dice2;
        }
    }
}
