﻿using CustomMonopoly.DomainClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomMonopoly
{
    public class Player
    {
        public int CurrentSpace { get; set; } = 0;
        public BoardSpace CurrentSpaceProperty { get; set; } = MonopolyGame.Spaces[0];
        public string DisplayCounter { get; set; }
        public string PlayerName { get; set; }
        private double Balance { get; set; }
        public List<BoardSpace> Properties { get; set; }

        public Player(string Name, double Balance = 0)
        {
            PlayerName = Name;
            this.Balance = Balance;
        }

        public double GetBalance()
        {
            return Balance;
        }

        public bool Charge(int n)
        {
            if (Balance >= n)
            {
                Balance -= Balance >= n ? n : 0;
                Console.WriteLine($"Charged {PlayerName} {n} new balance {Balance}");
                return true;
            }
            Console.WriteLine($"Charge: {PlayerName} does not have enough money ({Balance})");
            return false;
        }

        public void ToString()
        {
            Console.WriteLine($"Player (Name = {PlayerName}, CurrentSpace = {CurrentSpace}");
        }

    }
}
