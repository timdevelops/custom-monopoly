﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomMonopoly.DomainClasses
{
    public static class Colour
    {
        public static Color Brown = Color.SaddleBrown;
        public static Color LightBlue = Color.LightBlue;
        public static Color Pink = Color.Magenta;
        public static Color Orange = Color.Orange;
        public static Color Red = Color.Red;
        public static Color Yellow = Color.Yellow;
        public static Color Green = Color.DarkGreen;
        public static Color DarkBlue = Color.DarkBlue;
        public static Color Black = Color.Black;
    }
}
