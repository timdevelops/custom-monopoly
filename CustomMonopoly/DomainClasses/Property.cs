﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomMonopoly.DomainClasses
{
    public class Property : BoardSpace
    {
        public int Price { get; set; }
        public Player Owner { get; set; }
        public Color Colour { get; set; }
        public int[] Rents { get; set; } = new int[6];

        public Property(string title, Color colour, int price, int[] rents) : base(title)
        {
            Price = price;
            Colour = colour;
            Rents = rents;
        }

        public Color GetColour()
        {
            return Colour;
        }

        public bool Purchase(Player p)
        {
            if (Owner == null)
            {
                if (p.Charge(Price))
                {
                    Console.WriteLine($"{Title} was bought by {p.PlayerName}");
                    Owner = p;
                    return true;
                }
                Console.WriteLine($"Sorry, this property costs {Price}. You only have {p.GetBalance()}");
                return false;
            }
            else
            {
                Console.WriteLine($"This property is already owned by {Owner.PlayerName}");
                return false;
            }
        }

        public void ToString()
        {
            Console.WriteLine($"Property (Title = {Title}, Owner = {Owner}, Price = {Price}, SpaceId = {SpaceId}, Type = {Type})");
        }
    }
}
