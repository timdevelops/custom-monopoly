﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomMonopoly.DomainClasses
{
    public enum PropertyType
    {
        Residential = 1,
        Utility = 2,
        Chance = 3,
        CommunityChest = 4,
        Go = 5,
        Tax = 6,
        SuperTax = 7,
        Jail = 8,
        GoToJail = 9,
        FreeParking = 10
    }
}
