﻿using CustomMonopoly.DomainClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomMonopoly
{
    public partial class Form1 : Form
    {
        MonopolyGame GameClass { get; }
        bool isProperty { get; set; } = false;
        public Form1()
        {

            InitializeComponent();
            GameClass = new MonopolyGame();
            NewGameSetup();
        }
        public void NewGameSetup()
        {
            label1.Text = GameClass.GetCurrentTurn().PlayerName;
            UpdatePropertyDetails();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public void UpdateTurnOfLabel(string Name)
        {
        }

        private void UpdatePropertyDetails()
        {
            isProperty = GameClass.GetCurrentTurn().CurrentSpaceProperty is Property;
            BoardSpace prop = GameClass.GetCurrentTurn().CurrentSpaceProperty;
            PropertyOwnerLabel.Text = isProperty ? ((Property)prop).Owner != null ? ((Property)prop).Owner.PlayerName : "Not currently owned" : "N/A";
            pictureBox1.Refresh();
            pictureBox1.Visible = isProperty ? true : false;

            CurrentPropertyLabel.Text = GameClass.GetCurrentTurn().CurrentSpaceProperty.Title;

            UpdateBalance();

            CurrentPropertyLabel.Visible = !isProperty;
            if (isProperty)
            {
                Property propobj = (Property)prop;
                PropertyCostLabel.Text = "£" + propobj.Price;
                if (propobj.Owner==null)
                {
                    button2.Enabled = true;
                }
                else
                {
                    button2.Enabled = false;
                }
            }
            else
            {
                button2.Enabled = false;
                PropertyCostLabel.Text = "N/A";
            }

            if (prop is TaxSpace)
            {
                TaxSpace taxprop = (TaxSpace)prop;
                MessageBox.Show("Nothing in life is certain except death and taxes."+"\n"+"Time for you to pay up."+"\n"+$"£{taxprop.Cost}, please.", taxprop.Title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                GameClass.GetCurrentTurn().Charge(taxprop.Cost);
                UpdateBalance();
            }
        }

        public void UpdateBalance()
        {
            label8.Text = $"£{GameClass.GetCurrentTurn().GetBalance()}";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DiceRoll roll = GameClass.RollDice();
            label4.Text = roll.Dice1.ToString() + ", " + roll.Dice2.ToString() + " - " + roll.Total.ToString();

            UpdatePropertyDetails();
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Pen bp = new Pen(Color.Black, 3);

            SolidBrush br;

            if (isProperty)
            {
                int Rent1 = 0;
                int Rent2 = 0;
                int Rent3 = 0;
                int Rent4 = 0;
                int Rent5 = 0;
                int Rent6 = 0;

                if (GameClass.GetCurrentTurn().CurrentSpaceProperty is Property)
                {
                    int[] rents = ((Property)GameClass.GetCurrentTurn().CurrentSpaceProperty).Rents;
                    Rent1 = rents[0];
                    Rent2 = rents[1];
                    Rent3 = rents[2];
                    Rent4 = rents[3];
                    Rent5 = rents[4];
                    Rent6 = rents[5];
                    br = new SolidBrush(((Property)GameClass.GetCurrentTurn().CurrentSpaceProperty).GetColour());
                }
                else
                {
                    br = new SolidBrush(Color.Black);
                }


                e.Graphics.DrawRectangle(bp, 0, 0, 250, 300);
                e.Graphics.FillRectangle(br, 0, 0, 252, 45);
                e.Graphics.DrawRectangle(bp, 10, 185, 230, 105);

                using (Font myFont = new Font("Arial", 14))
                {
                    e.Graphics.DrawString(GameClass.GetCurrentTurn().CurrentSpaceProperty.Title, myFont, Brushes.White, new Point(13, 13));
                    e.Graphics.DrawString("RENT  -  site only      £" + Rent1.ToString(), myFont, Brushes.Black, new Point(13, 55));
                    e.Graphics.DrawString("              1 house       £" + Rent2.ToString(), myFont, Brushes.Black, new Point(13, 75));
                    e.Graphics.DrawString("              2 houses     £" + Rent3.ToString(), myFont, Brushes.Black, new Point(13, 95));
                    e.Graphics.DrawString("              3 houses     £" + Rent4.ToString(), myFont, Brushes.Black, new Point(13, 115));
                    e.Graphics.DrawString("              4 houses     £" + Rent5.ToString(), myFont, Brushes.Black, new Point(13, 135));
                    e.Graphics.DrawString("              HOTEL        £" + Rent6.ToString(), myFont, Brushes.Black, new Point(13, 155));

                    e.Graphics.DrawString("Property Costs", myFont, Brushes.Black, new Point(13, 194));
                    e.Graphics.DrawString("Houses - £200 each", myFont, Brushes.Black, new Point(13, 225));
                    e.Graphics.DrawString("Hotel  - £200", myFont, Brushes.Black, new Point(13, 245));
                    e.Graphics.DrawString("            PLUS 4 houses", myFont, Brushes.Black, new Point(13, 265));
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (isProperty)
            {
                Property prop = (Property)GameClass.GetCurrentTurn().CurrentSpaceProperty;
                DialogResult dialogResult = MessageBox.Show($"Do you want to purchase {prop.Title} for {prop.Price}?", "Confirm purchase", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    if (!prop.Purchase(GameClass.GetCurrentTurn()))
                    {
                        MessageBox.Show("You don't have enough money to buy this property.", "Deal fell through", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    }
                    else
                    {
                        MessageBox.Show($"Woohoo! You bought {prop.Title}", "Exchanged keys!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            UpdatePropertyDetails();
        }
    }
}
